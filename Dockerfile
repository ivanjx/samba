FROM arm64v8/ubuntu:focal

# Install samba.
RUN apt-get update && \
    apt-get install -y samba

# Disable smbd service.
RUN /etc/init.d/smbd stop && \
    rm /etc/init.d/smbd

# Config file template and entrypoint.
COPY ./start.sh ./smb.conf.template /
RUN chmod +x /start.sh

# Done.
ENTRYPOINT [ "/start.sh" ]
