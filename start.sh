#!/bin/bash

# Args:
# - USER
# - PASSWORD
# - SERVERSTRING
# - SHARENAME

# Creating user.
useradd --no-create-home $USER
printf "${PASSWORD}\n${PASSWORD}\n" | smbpasswd -s -a $USER

# Creating share directory.
mkdir -p /data
chown -R root /data

# Copy config template.
cp /smb.conf.template /etc/samba/smb.conf

# Setting up config.
sed -i "s/SERVERSTRING/${SERVERSTRING}/" /etc/samba/smb.conf
sed -i "s/SHARENAME/${SHARENAME}/" /etc/samba/smb.conf
sed -i "s/SHAREUSERNAME/${USER}/" /etc/samba/smb.conf

# Starting smbd.
exec smbd --foreground --log-stdout --no-process-group
